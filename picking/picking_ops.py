import bpy
from bpy.props import *
from . picking_maths import pick_material_under

class GPCOLORMAP_OT_pickMaterial(bpy.types.Operator):
    bl_idname = "scene.colormap_pick_material"
    bl_label = "Pick from viewport"
    bl_description = "Pick material from viewport"

    bl_options = {'REGISTER', 'DEPENDS_ON_CURSOR', 'UNDO'}
    bl_cursor_pending = 'EYEDROPPER'

    def execute(self, context):  
        return {'FINISHED'}   

    def draw(self, context):
        layout = self.layout
        gpcm = context.scene.gpcolormapping
        item = gpcm.active_mapping()
        if (item is None) or (item.base_material is None):
            return
        mat = item.base_material

        row = layout.row()

        col = row.column()
        col.ui_units_x = 2
        col.prop(item, 'group', text='', emboss=False)
        col.enabled = item.is_active

        col = row.column()
        col.ui_units_x = 4
        col.enabled = item.is_active
        col.label(text=item.name, icon_value=mat.id_data.preview.icon_id)

        col = row.column()
        tpv_icon = 'RESTRICT_RENDER_OFF' if item.is_active else 'RESTRICT_RENDER_ON'
        col.prop(item, 'is_active', icon=tpv_icon, text='', emboss=False)

        show_stroke = mat.grease_pencil.show_stroke
        show_fill = mat.grease_pencil.show_fill
        is_linked = item.is_linked

        if show_stroke and show_fill:
            col = row.column()
            col.enabled = item.is_active
            lkd_icon = 'LINKED' if is_linked else 'UNLINKED'
            col.prop(item, 'is_linked', icon=lkd_icon, text='', emboss=False)

        col = row.column()
        row = col.row()
        if show_stroke:
            row.prop(item, "stroke_color", text="")         

        if show_fill and ((not is_linked) or (not show_stroke)):            
            row.prop(item, "fill_color", text="")   

        col.enabled = item.is_active
        col.ui_units_x = 5

    def invoke(self, context, event):
        self.cursor = event.mouse_region_x, event.mouse_region_y

        gpcm = context.scene.gpcolormapping            
        picked_mat = pick_material_under(context, self.cursor)
        if picked_mat is None:
            return {'CANCELLED'}

        map_ind = gpcm.index(picked_mat)
        if map_ind < 0:
            # Mapping do not exist => create it !
            gpcm.pending_material = picked_mat
            if not gpcm.accept_pending(context, set_active=True):
                self.report({'WARNING'}, "Mapping was not accepted")
                return {'CANCELLED'}
        else:
            # Mapping exists => set as active
            gpcm.active_index = map_ind

        return context.window_manager.invoke_popup(self)

classes = [GPCOLORMAP_OT_pickMaterial]

def register():
    for cls in classes:
        bpy.utils.register_class(cls) 

def unregister():        
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)