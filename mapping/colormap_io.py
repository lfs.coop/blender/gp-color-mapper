import bpy
from .. common.color import rgba2hex, hex2rgba

def export_mappings_content(context):    
    import datetime as dt
    data = {"__meta__" : {"timestamp": str(dt.datetime.now())}}

    gpcm = context.scene.gpcolormapping
    for mapping in gpcm.mappings:
        data[mapping.name] = {}
        data[mapping.name]["active"] = mapping.is_active
        data[mapping.name]["linked"] = mapping.is_linked

        data[mapping.name]["fill_color"] = rgba2hex(mapping.fill_color)
        data[mapping.name]["stroke_color"] = rgba2hex(mapping.stroke_color)

        data[mapping.name]["group"] = mapping.group

    return data

def import_mappings_content(context, data):
    materials = bpy.data.materials
    gpcm = context.scene.gpcolormapping

    for mname, mdat in data.items():
        if mname.startswith("__"):
            continue
        
        if not mname in materials:
            print(f"WARNING : Material {mname} not found in file")
            continue
        
        if mname in gpcm.mappings:
            mapping = gpcm.mappings[mname]
        else:
            mapping = gpcm.mappings.add()
            mapping.base_material = materials[mname]

        def parse_color(color_data):
            if isinstance(color_data, str):
                return hex2rgba(color_data)
            return color_data

        mapping.fill_color = parse_color(mdat["fill_color"])
        mapping.stroke_color = parse_color(mdat["stroke_color"])

        mapping.is_active = mdat["active"]
        mapping.is_linked = mdat["linked"]
        mapping.group = mdat["group"]
