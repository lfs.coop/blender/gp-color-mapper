# Grease Pencil Color Mapper

Blender add-on to map a set of grease pencil materials to new colors.

## General use
The add-on is controlled by a panel available in the Scene Properties panel, under 'Grease Pencil Color Mapper'.
Start by adding a mapping by clicking the '+' button, and choosing a material.
By default a mapping is added in the same color as the material, and thus you will not see any change until you change the target color of the mapping.

![Add single mapping](doc/mapper_add_single_mapping.png "Add single mapping")

Beware that the target colors can only be seen in viewports set to 'Material preview' or 'Render preview' mode. 

The mapping are done by adding grease pencil modifiers into each grease pencil objects using the chosen material. You can see them in the list of grease pencil modifiers.

## Operations

![Overview of possible operations in the mapper](doc/mapper_operations.png "Operations in the mapper")

There is a list of possible operations on mappings : 
 - **APPLY**  : change material to comply with target colors, removes the mapping after changing material. 'Apply all' also removes inactive mappings.
 - **ACTIVE** : toggle visibility (in viewport and rendering).
 - **LINK/UNLINK** : when a material has both stroke and fill colors, you can either chose a unique target color to map both(linked) or two separate target colors to map each (unlinked).
 - **REMOVE** : remove mapping and corresponding modifiers. 
 - **REFRESH** : loops over grease pencil objects to add or adjust all the modifiers. This is useful in the case of an object created after a mapping or if one of the modifier was removed or modified manually by error.
 - **ADD MAPPING** : adds and initializes a mapping from an input material. Alternatively, you can *add all materials in active object* or *all materials* in the file. Note that mappings only work with grease pencil materials.
 - **RESET ALL** : sets each target color to the color of its initial material.
 - **PRESETS** : additionally, we started developping a series of predefined mappings. So far, there is only one which converts all colors into grayscale values. Presets only work on the target colors of existing mappings.
 - **IMPORT/EXPORT** : mappings can be exported and imported in Blender as JSON files. Materials will not be embedded in the export, so the import will only include mappings for which material already exists in the file.

## Known limitations
The mappings can currently only occur in "solid" materials. It does not take into account texture or gradients.

## License
Published under GPLv3 license.
